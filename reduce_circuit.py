from base import *
from compressAO import *
from convertNAND import *

def crosslink(dag):
    # TODO: see what happens on repeat calls.
    # does not use the with x as _: tactic
    if not isinstance(dag, Dag):
        return dag

    # the copying is the main bottleneck
    u = dag.copy()

    a = {}
    def rc(obj):
        for x in obj.L:
            if not isinstance(x, Dag) or x.v == True:
                continue
            if x in a:
                # merge
                p = a[x]
                p.post += x.post
                for j in x.post:
                    j.L[j.L.index(x)] = p
            else:
                a[x] = x
                rc(x)
            x.v = True
    with u as u:
        rc(u)
    return u

def compressToCircuit(data, nand=False):
    def stepcheck(x,buf=[0]):
        buf[0] += 1
        x = convert(x, DATA)
        #print()
        #print(buf[0])
        if Data(x) != (Data(data)):
            printf("{} != {}", x, data)

    stepcheck(data)
    x =  convert(data, FORMULA)
    stepcheck(x)
    x = compressToAO(x)
    stepcheck(x)
    if nand:
        x = toBinaryGates(x)
        stepcheck(x)
        x = convNAND2(x)
        stepcheck(x)
    x = convert(x, CIRCUIT)
    stepcheck(x)
    x = crosslink(x)
    stepcheck(x)
    z2 = convert(x, DATA)
    stepcheck(x)
    return x


