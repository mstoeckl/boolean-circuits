from base import *
from compressAO import compressToAO
from convertNAND import toNAND, toBinaryGates
from reduce_circuit import crosslink, compressToCircuit

import random, time


def evalFormula(s):
    x = []
    for v in convert(s, TEXT, DATA):
        x.append(str(v))
    return " ".join(x)

def enableNicePrinting():
    Matrix.__str__ = matrix_print
    pass
    #Box.converters[FORMULA] = lambda x: convert(x, TEXT)

def eqv(a, b):
    return convert(a, DATA) == convert(b, DATA)

def gateSize(x):
    return toBinaryGates(convert(x, FORMULA)).getGateCount()

def printf(x, *a, **b):
    print(x.format(*a, **b))

def comloop(sz):
    avg = 0
    c = 0
    mxy = 0
    mxz = 0
    for v in boolvecs(2 ** sz):
        y = toBinaryGates(compressToAO(v))
        z = cancelNAND(convToNAND2(y))
        a,b = y.getGateCount(), z.getGateCount()
        mxy = max(mxy, a)
        mxz = max(mxz, b)
        print(y)
        if (b != 0):
            c += 1
            avg = (avg * c + (a / b)) / (c + 1)
    return avg, mxy, mxz, mxz/mxy



def formallyPrintData(x):
    n = 0
    while 2 ** n < len(x):
        n += 1
    print("A B: F")
    for i in range(len(x)):
        printf("{}: {}", " ".join(map(str,int2binl(i, n))), x[i])

def listUnicode(n):
    for a in range(n):
        b = hex(a)[2:]
        c = "\"\\u"+"0"*(4-len(b)) + b+ "\""
        d = eval(c)
        if d.isupper():
            print(d, end="")
    print()

rvec = lambda g : [random.choice([0, 1]) for x in range(2 ** g)]
CD = lambda x: convert(x, DATA)
CC = lambda x: convert(x, CIRCUIT)
CM = lambda x: convert(x, MATRIX)
CF = lambda x: convert(x, FORMULA)

def repapply(x, f, n):
    for _ in range(n):
        x = f(x)
    return x

def timeit(func):
    a = time.time()
    func()
    printf("Took {} secs.",time.time() - a)

def negateit(d):
    # very funky behavior. Maybe transform_L is perfect..
    def t(u):
        return Dag(NOT, u)
    def s(u):
        return Dag(NOT, u)
    def r(u):
        return {"T":"F","F":"T"}[u]
    return dag_transform_L(d, t,s,r)

def blindnand(d):
    def t(u):
        if u.F == NOT:
            return Dag(NAND2, u.L[0], u.L[0])
        if u.F == AND:
            v = Dag(NAND2, u.L[0], u.L[1])
            return Dag(NAND2, v, v)
        if u.F == OR:
            return Dag(NAND2, Dag(NAND2, u.L[0], u.L[0]),
                                Dag(NAND2, u.L[1], u.L[1]))
        raise Exception()

    return dag_transform_L(d, t)

def repeat(func, n):
    def rep():
        for _ in range(n):
            func()
    return rep

def timexn(func, n):
    r = range(n-1)
    a = time.time()
    for x in r:
        func()
    b = func()
    printf("Took {} secs.",time.time() - a)
    return b

# until we have good infrastructure, let us focus only on AOI reduction
# NAND comes _later_
def main():
    enableNicePrinting()

    x = rvec(5)
    a = CC(x)
    b = CM(x)

    a = timexn(lambda:crosslink(a), 100)
    b = timexn(lambda:matrix_crosslink(b), 100)

    pd = lambda x: print(CD(x), x)

    #pd(a)
    #pd(CC(b))
    #pd(CC(CM(a)))

    #ts = lambda x: timeit(repeat(lambda:CD(x), 2))
    #ts(b)
    #ts(c)

    # TODO: fix all TODO cases!
