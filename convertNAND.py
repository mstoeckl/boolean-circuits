from base import *

def naryCombine(a, bnd):
    """ Arity independent soln.
    Requires the function bnd to be associative"""
    k = len(a)
    if k == 1:
        return a[0]
    if k <= bnd.getArity():
        return xtuple(bnd, list(a))

    r = bnd.getArity()
    n = k / r
    u = []
    for x in range(r):
        u.append(naryCombine( a[ x * k // r : (x+1) * k // r ], bnd))
    return xtuple(bnd, u)

def toBinaryGates(x):
    def trans(u):
        if fhead(u) not in (AND, OR):
            return u

        nk = {AND:AND2, OR:OR2}[head(u)]
        return naryCombine(tail(u), nk)
    return recTransL(x, trans)

def toNAND(x):
    def negate(u):
        if isForm(u):
            ar = tail(u)
            if ar[0] == ar[1]:
                return ar[0]

        return (NAND2, u, u)

    def trans(u):
        if not isForm(u):
            return u
        key = head(u)
        args = tail(u)

        if key == NOT:
            return negate(args[0])

        a0, a1 = args

        if key == AND2:
            return negate((NAND2, a0, a1))

        if key == OR2:
            return (NAND2, negate(a0), negate(a1))

        raise Exception("Need AOI2 formula: Found Gate {}".format(key))

    return recTransL(x, trans)
