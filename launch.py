#!/usr/bin/env python3

if __name__ == "__main__":
    import sys
    import circuits
    if len(sys.argv) == 2 and sys.argv[1] == "-c":
        import cProfile
        loc = "profile.dat"
        cProfile.run("circuits.main()", loc)
        import pstats
        data = pstats.Stats(loc, stream=sys.stderr)
        data.sort_stats("time")
        data.print_stats()
        # TODO: ye gods, pstats sucks. All I want is:

        #
        # 100.0000% main: blah.py:67
        #  - 50.0001% foo: meh.py:128
        #  - 49.9999% munch: nix.py:1

        # So, a tree visualization of the callchain and percentage time (abs, relative)
        # and be able to invert the tree to find callers of the critical function
    elif len(sys.argv) == 2 and sys.argv[1] == "-p":
        import statprof
        class Statprof():
            def __init__(self, freq):
                statprof.reset(freq)
            def __enter__(self):
                statprof.start()
                return None
            def __exit__(self, x, y, z):
                statprof.stop()
                statprof.display(sys.stderr)
                return False
        with Statprof(1000) as _:
            circuits.main()


    else:
        circuits.main()
else:
    raise Exception("Don't import me!")
