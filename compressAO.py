from base import *

def negateTerm(t):
    if fhead(t) == NOT:
        return t[1]
    else:
        return (NOT, t)

def getLinks(inputs, targ):
    """
    Returns an dict of values mapping to lists
    of sets containing those values
    """
    cs = {}
    for inp in inputs:
        l = list()
        for ix in range(len(targ)):
            if inp in targ[ix]:
                l.append(ix)
        if len(l):
            cs[inp] = l
    return cs

def compressDistr(x):
    """ Recursively group stuff to put in POS form.
    Takes a formula."""
    def proc(u):
        malformed = [x for x in tail(u) if fhead(x) != AND]
        v = [tail(x) for x in tail(u) if fhead(x) == AND]
        v = list(map(set,v))

        if not v:
            return u, False

        cs = getLinks(set().union(*v), v)

        ml = 0
        for x in cs:
            if len(cs[x]) > ml:
                ml = len(cs[x])
                key = x
        if ml <= 1:
            return u, False

        lnk = []
        ul = []
        for x in v:
            if key in x:
                x.remove(key)
                t = lnk
            else:
                t = ul

            if len(x) > 1:
                t.append(xtuple(AND, list(x)))
            elif len(x)  == 1:
                t += list(x)
            else:
                raise Exception("Shouldn't happen")
        s = (AND, key, xtuple(OR, lnk))
        nxt = [s] + ul + malformed
        if len(nxt) > 1:
            u = xtuple(OR, nxt)
        else:
            u = nxt[0]

        return u, True

    def trans(u):
        if fhead(u) != OR:
            return u

        k = True
        while k:
            u,k = proc(u)
        return u

    return recTransH(x, trans)

def compressRedundant(x):

    """ Remove redundancies: X+X'->1; X+1->0 """
    def trans(u):
        if not isForm(u):
            return u

        r  = tail(u)

        if head(u) == OR:
            if len(r) == 2:
                for a,b in ((r[0],r[1]),(r[1],r[0])):
                    nb = negateTerm(b)

                    if a == nb:
                        return "T"
                    if a == "T":
                        return "T"
                    if isForm(a) and head(a) == AND:
                        if tail(a).count(nb) > 0:
                            nn = [x for x in tail(a) if x != nb]
                            if len(nn) > 1:
                                return (OR, b, xtuple(AND, nn))
                            else:
                                return (OR, b, nn[0])

        if head(u) == AND:
            nn = []
            for k in r:
                if k != "T":
                    nn.append(k)
            if not nn:
                return "T"
            if len(nn) > 1:
                return xtuple(AND, nn)
            else:
                return nn[0]

        return u

    return recTransL(x, trans)

def compressNot(x):
    """ Recursively apply DeMorgan to push NOT gates up.
    Takes a formula."""

    def trans(u):
        if not isForm(u):
            return u

        if head(u) not in (AND,OR):
            return u

        neg = sum(1 for x in tail(u) if fhead(x) == NOT)
        pos = len(u) - neg - 1
        if neg - pos <= 1: # gain
            return u

        v = list(map(negateTerm, tail(u)))
        nk = {AND:OR,OR:AND}[head(u)]

        return (NOT, xtuple(nk, v))

    return recTransH(x, trans)

def flattenAO(x):
    """ Replace nested ANDs and ORs with flat versions """
    def trans(u):
        if not isForm(u):
            return u

        key = head(u)
        if key not in (AND, OR):
            return u

        nn = []
        for x in tail(u):
            if fhead(x) == key:
                nn += tail(x)
            else:
                nn.append(x)
        return xtuple(key, nn)
    return recTransL(x, trans)

def compressToAO(t):
    # TODO: do boxing/unboxing in here
    x = convert(t, DATA, FORMULA)
    app = [compressDistr, compressRedundant, flattenAO, compressNot]
    for fun in app:
        x = fun(x)
    return x
