from .functions import *
from .dag import *
from .formula import *

# Dirty code goes here! It relies on base-specific details,
# or non-extensible parsing

def t2f_rec_not(tree):
    if type(tree) is not list:
        if tree == "'":
            raise Exception("Unbound Not.")
        return tree
    n = []
    for v in tree:
        if v == "'":
            if not n:
                raise Exception("Not does not follow an operator")
            else:
                n[-1] = [NOT,  n[-1]]
        else:
            n.append(t2f_rec_not(v))
    return n

def t2f_rec_ao(tree):
    if type(tree) is not list:
        return tree

    if tree[0] == NOT:
        return [NOT,  t2f_rec_ao(tree[1])]

    n = [[]]
    for v in tree:
        if v == "+":
            if not n[-1]:
                raise Exception("Unbound and")
            n.append([])
        else:
            n[-1].append(t2f_rec_ao(v))

    for i in range(len(n)):
        if len(n[i]) == 1:
            n[i] = n[i][0]
        else:
            n[i] = [AND] + n[i]

    if len(n) == 1:
        return n[0]
    else:
        return [OR] + n

def t2f_rec_formula(head):
    if type(head) == list:
        x = []
        for u in head[1:]:
            x.append(t2f_rec_formula(u))
        return xtuple(head[0], x)
    elif head in LETTERS:
        return LETTERS.index(head)
    elif head == "1":
        return "T"
    elif head == "0":
        return "F"
    raise Exception("Odd")

def text2formula(s):
    # multipass: first grabs parens
    head = []
    stack = [head]
    parenstack = []

    pconv = {"[":"]", "{":"}",  "(":")"}

    for c in s:
        if c in "[({":
            stack.append([])
            stack[-2].append(stack[-1])
            parenstack.append(c)
        elif c in "])}":
            if not parenstack:
                raise Exception("Extra closeparen")
            if c != pconv[parenstack[-1]]:
                raise Exception("Closeparen does not match")
            parenstack.pop()
            stack.pop()
        elif c in LETTERS or c in "'+" or c in "01":
            stack[-1].append(c)
        elif c != " ":
            raise Exception("Not a legal character: |{}|".format(c))

    head = t2f_rec_not(head)
    head = t2f_rec_ao(head)
    head = t2f_rec_formula(head)
    return head

def formula2text(f):
    if not isForm(f):
        if isBool(f):
            if f == "T":
                return "1"
            return "0"
        else:
            return LETTERS[f]

    def up(*topar):
        def ret(x):
            t = formula2text(x)
            if isForm(x):
                if head(x) in topar and len(t) > 1:
                    return "({})".format(t)
            return t
        return ret

    def jmp(mx, topar, form):
        return mx.join(map(up(*topar), tail(form)))

    key = head(f)
    if key == AND:
        return jmp("", [OR], f)
    elif key == OR:
        return jmp(" + ", [], f)
    elif key == NOT:
        if len(f) == 2:
            return up(AND,OR)(f[1]) + "'"
        else:
            raise Exception("NOT took too many args")
    else:
        if key == AND2:
            return jmp("", [AND2, OR2], f)
        elif key == OR2:
            return jmp(" + ", [AND2, OR2], f)
        elif key == NAND2: # eqv to NAND->AOI
            return "["+jmp("", [AND2, OR2], f) +"]'"
        elif key == NOR2: # eqv to NAND->AOI
            return "["+jmp("+", [AND2, OR2], f) +"]'"
        else:
            raise Exception("Unexpected function")

def printDag(dag):
    def rsl(thing, undhf):
        # TODO: rsl is weak, can restructure to half size & double spd
        if thing == "T":
            return [TRUE]
        elif thing == "F":
            return [FALSE]
        elif isinstance(thing, int):
            return LETTERS[thing]

        li = [thing.F]
        for z in thing.L:
            if z == "T":
                li.append([TRUE])
            elif z == "F":
                li.append([FALSE])
            elif isinstance(z, int):
                li.append(LETTERS[z])
            elif isinstance(z, Dag):
                if len(z.post) > 1:
                    if z not in undhf[1]:
                        undhf[0].append(z)
                        undhf[1][z] = len(undhf[0]) - 1
                    li.append(undhf[1][z])
                else:
                    li.append(rsl(z, undhf))
            else:
                raise Exception("Odd type")
        return li

    def rp(u):
        if type(u) == int:
            return LOWER_LETTERS[u]
        return u

    def pr_sect(sect):
        if type(sect) != list:
            return sect

        # TODO: solve generalized printing problem ;-)

        h = sect[0]
        if h == AND:
            return "("+"".join(pr_sect(x) for x in sect[1:])+")"
        if h == OR:
            return "("+" + ".join(pr_sect(x) for x in sect[1:])+")"
        if h == NOT:
            return pr_sect(sect[1]) + "'"
        if h == TRUE:
            return "1"
        if h == FALSE:
            return "0"
        if h == NAND2:
            return "["+"".join(pr_sect(x) for x in sect[1:])+"]'"
        raise Exception(h)

    mdeps = [[], {}]

    head = rsl(dag, mdeps)

    ul = []
    count = 0
    while count < len(mdeps[0]):
        ul.append(rsl(mdeps[0][count], mdeps))
        count += 1

    sk = []
    for i in range(len(ul)):
        sk.append("{}:={}".format(LOWER_LETTERS[i],
                                    pr_sect(treeReplaceLeaves(rp, ul[i]))))
    sk.append(pr_sect(treeReplaceLeaves(rp,  head)))

    return "{" + "; ".join(sk) + "}"

Dag.__str__ = printDag
