LETTERS = \
  "ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞĀĂĄĆĈĊČĎĐĒĔĖĘĚĜĞĠĢĤĦĨĪĬĮİĲĴĶĹĻĽĿŁŃŅŇŊŌŎŐŒŔŖŘŚŜŞŠŢŤŦŨŪŬŮŰŲŴŶŸŹŻŽƁƂƄƆƇƉƊƋƎƏƐ" + \
  "ƑƓƔƖƗƘƜƝƟƠƢƤƦƧƩƬƮƯƱƲƳƵƷƸƼǄǇǊǍǏǑǓǕǗǙǛǞǠǢǤǦǨǪǬǮǱǴǶǷǸǺǼǾȀȂȄȆȈȊȌȎȐȒȔȖȘȚȜȞȠȢȤȦȨȪȬȮȰȲȺȻȽȾɁɃɄɅɆɈɊɌɎͰͲͶΆΈΉΊΌΎΏΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩΪΫϏϒϓϔ" + \
  "ϘϚϜϞϠϢϤϦϨϪϬϮϴϷϹϺϽϾϿЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯѢѲҐҒҔҖҘҚҢҤҪҬҮҰҲҺӀӁӃӇӋӐӒӔӖӘӚӜӞӠӢӤӦӨӪӬӮӰӲӴӶӸԐԔԖԘԚԜԞԠԢԤԦԱԲԳԴԵԶԷԸԹԺԻԼ" + \
  "ԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖḀḂḄḆḈḊḌḎḐḒḔḖḘḚḜḞḠḢḤḦḨḪḬḮḰḲḴḶḸḺḼḾṀṂṄṆṈṊṌṎṐṒṔṖṘṚṜṞṠṢṤṦṨṪṬṮṰṲṴṶṸṺṼṾẀẂẄẆẈẊẌẎẐẒẔẞẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼẾỀỂỄỆỈỊỌỎỐỒỔỖỘỚ" + \
  "ỜỞỠỢỤỦỨỪỬỮỰỲỴỶỸỺỼỾἈἉἊἋἌἍἎἏἘἙἚἛἜἝἨἩἪἫἬἭἮἯἸἹἺἻἼἽἾἿὈὉὊὋὌὍὙὛὝὟὨὩὪὫὬὭὮὯᾸᾹᾺΆῈΈῊΉῘῙῚΊῨῩῪΎῬῸΌῺΏℂℇℋℌℍℐℑℒℕℙℚℛℜℝℤΩℨKÅℬℭℰℱℲℳℾℿⅅⅠⅡⅢⅣⅤⅥⅦⅧⅨⅩⅪⅫⅬⅭⅮⅯↃⰀⰁ" + \
  "ⰂⰃⰄⰅⰆⰇⰈⰉⰊⰋⰌⰍⰎⰏⰐⰑⰒⰓⰔⰕⰖⰗⰘⰙⰚⰛⰜⰝⰞⰟⰠⰡⰢⰣⰤⰥⰦⰧⰨⰩⰪⰫⰬⰭⰮⱠⱢⱣⱤⱧⱩⱫⱭⱮⱯⱰⱲⱵⱾⱿⲀⲂⲄⲆⲈⲊⲌⲎⲐⲒⲔⲖⲘⲚⲜⲞⲠⲢⲤⲦⲨⲪⲬⲮⲰⲲⲴⲶⲸⲺⲼⲾⳀⳂⳄⳆⳈⳊⳌⳎⳐⳒⳔⳖⳘⳚⳜⳞⳠⳢ"
LOWER_LETTERS = LETTERS.lower()
def treeGetUnique(match, tree):
    if type(tree) == list:
        x = set()
        for v in tree:
            x = x.union(treeGetUnique(match, v))
        return x
    else:
        if match(tree):
            return set(tree)
        return set()

def treeCountUnique(match,  tree):
    return len(treeGetUnique(match, tree))

def treeReplaceLeaves(func, tree):
    if type(tree) == list:
        return list(map( lambda x: treeReplaceLeaves(func, x), tree))
    return func(tree)

class Wrapper():
    def __init__(self,  lk):
        self.l = lk
    def inc(self):
        self.l[0] += 1
        return self.get()
    def get(self):
        return self.l[0]

def bin2int(b):
    i = 0
    for x in range(len(b)-1, -1, -1):
        i += (2 ** x) * b[x]
    return i


def int2bin(n):
    i = 0
    while 2 ** i <= n:
        i += 1

    v = [0] * i
    l = i

    while n > 0:
        u = 2 ** i
        if u <= n:
            n -= u
            v[l-i-1] = 1
        i -= 1

    return v
    
def prepad(leng,  val,  given):
    diff = leng - len(given)
    return [val] * diff + given

def int2binl(i, l):
    return prepad(l, 0,  int2bin(i))

def boolvecs(n):
    for i in range(2 ** n):
        yield prepad(n, 0,  int2bin(i))

def treeiter(t):
    if type(t) == list:
        for x in t:
            yield from treeiter(x)
    else:
        yield t

def pairs(gen):
    i = False
    u = None
    for x in gen:
        if i:
            yield (u, x)
        else:
            i = True
        u = x

def head(x):
    return x[0]
def tail(x):
    return x[1:]

def clog2(x):
    i = 0
    while 2 ** i < x:
        i += 1
    return i

def lscale(arr, fact):
    mx = []
    for z in arr:
        mx += [z] * fact
    return mx
