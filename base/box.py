# TODO create an @Boxed() annotation, for easy use
# TODO: only use it when subtypes and metadata become a problem.

# TODO: think of an AUTO type...

TEXT = "TEXT"
FORMULA = "FORMULA"
CIRCUIT = "CIRCUIT"
DATA = "DATA"

class Box():
    def __init__(self, x, type, *flags):
        self.x = x
        if type in (TEXT, FORMULA, CIRCUIT, DATA):
            self.type = type
        else:
            raise Exception("Not an appropriate type to box with")
        self.flags = set(flags)
    def __repr__(self):
        return "[[" + str(self) + "]]"
    def __str__(self):
        return converters[self.type](self.x)

    # how to make these easy to override? use a table, like convert
    converters = {
        TEXT:lambda x: x,
        FORMULA: lambda x: repr(x),
        CIRCUIT: lambda x: repr(x),
        DATA: lambda x: "{"+" ".join(map(str, x)) + "}"
        }

def isbox(x):
    return isinstance(x, Box)

def box(x, *args, **kwargs):
    return Box(x, *args, **kwargs)

def unbox(x, type=None, *flags):
    if not isinstance(x, Box):
        raise Exception("Object {} not in a box".format(x))
    if (type == None or type == x.type) and set(flags).issubset(x.flags):
        return x.x
    raise Exception("Object {} failed unboxing. Had {}, Needed {}".format(x, [x.type]+list(x.flags), [type] + list(flags)))

def boxed(pretype, posttype=None):
    if posttype is None:
        posttype = pretype
    def decor(func):
        def refunc(primary, *args, **kwargs):
            return Box(func(unbox(primary, pretype), *args, **kwargs), posttype)
        return refunc
    return decor

@boxed(TEXT, CIRCUIT)
def reboxEx(u):
    return u




