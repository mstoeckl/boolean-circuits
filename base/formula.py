from .functions import *

# TODO: make interface closer to Circuit, maybe create
# a top-down and bottom-up generator for transformations
# on each

# Formula code:
#
# (AND, 0, (OR, 1, "F")) -> (AND, 0)
#
def isFunction(x):
    return isinstance(x, Function)
def isBool(x):
    return x in ("T","F")
def isInput(x):
    return isinstance(x, int)
def isForm(x):
    return isinstance(x, tuple)
def isFormula(x):
    return isBool(x) or isInput(x) or isForm(x)


def xtuple(head, rest):
    return tuple([head] + rest)

def fsize(x):
    if isForm(x):
        return 1 + sum(fsize(y) for y in tail(x))
    return 0
def fwidth(x):
    if isForm(x):
        return max(fwidth(u) for u in tail(x))
    if isInput(x):
        return x + 1
    return 0
def fhead(x):
    if isForm(x):
        return head(x)
    return x

def recTransH(u, func):
    """ Recursively transform formula from root down"""
    v = func(u)
    if not isForm(v):
        return v

    n = [head(v)]
    for x in tail(v):
        n.append(recTransH(x, func))
    return tuple(n)

def recTransL(u, func):
    """ Recursively transform formula from leaves up"""
    if isForm(u):
        n = [head(u)]
        for x in tail(u):
            n.append(recTransL(x, func))
        return func(tuple(n))

    return func(u)
