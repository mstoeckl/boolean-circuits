from .aux import *

class Function():
    def __init__(self, name, arity, func, symbol=None):
        """
        Arity is integral, or if 0 can be anything positive
        """
        self._arity = arity
        self._func = func
        self._name = name
        if symbol is None:
            symbol = name
        self._symbol = symbol
    def __call__(self, inputs):
        return self._func(inputs)
    def __repr__(self):
        return "<{}>".format(self._name)
    def acceptArity(self, n):
        if self._arity > 0:
            return self._arity == n
        else:
            return n > 0
    def fixedArity(self):
        return self._arity > 0
    def getArity(self):
        return self._arity

def makeFuncNary(name, symb, args):
    length = len(args)
    i = 0
    while 2 ** i < length:
        i += 1
    if 2 ** i != length:
        raise Exception("Function \"{}\" data list length is not power of 2".format(name))
    def call(inputs):
        return args[bin2int(inputs)]
    return Function(name, i, call, symbol=symb)

def makeFuncGen(name, low, mixed, high):
    def call(inputs):
        h = 0
        l = 0
        for x in inputs:
            if x:
                h += 1
            else:
                l += 1
        if h and not l:
            return high
        elif l and not h:
            return low
        else:
            return mixed
    return Function(name, 0, call)

def compose(name, circuit):
    return Function(name, circuit.width(), lambda x: circuit(x))

AND2 = makeFuncNary("AND2", "",(0,0,0,1))
OR2 = makeFuncNary("OR2", " + ",(0,1,1,1))
NAND2 = makeFuncNary("NAND2", "⊼",  (1,1,1,0))
NOR2 = makeFuncNary("NOR2", "⊽", (1,0,0,0))

NOT = makeFuncNary("NOT", "!", (1,0))
ID = makeFuncNary("ID", "", (0, 1))

AND = makeFuncGen("AND", 0,0,1)
OR = makeFuncGen("OR", 0,1,1)
TRUE = makeFuncGen("TRUE", 1, 1, 1)
FALSE = makeFuncGen("FALSE", 0, 0, 0)

EQ = makeFuncGen("EQ", 0,1,0)
XOR = makeFuncGen("XOR", 1,0,1)

BASE_AOI2 = (AND2, OR2, NOT)
BASE_AOI = (AND, OR, NOT)
BASE_PARITY = (EQ, XOR)
