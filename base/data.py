from .aux import *

class Data(list):
    def __eq__(self, o):
        if not isinstance(o, list):
            return False

        if len(o) > len(self):
            r = len(o) // len(self)
            i = clog2(r)
            if 2 ** i != r:
                return False
            return list.__eq__(o, lscale(self, r))
        elif len(o) < len(self):
            r = len(self) // len(o)
            i = clog2(r)
            if 2 ** i != r:
                return False
            return list.__eq__(self, lscale(o, r))
        else:
            return list.__eq__(self, o)
    def __ne__(self, o):
        return not self.__eq__(o)
    def __repr__(self):
        return "⎡{}⎦".format(" ".join(map(str, self)))
    def __str__(self):
        tf = lambda x:"◌●"[x]
        return "⎰{}⎱".format("".join(map(tf, self)))

def isData(x):
    if not isinstance(x, list):
        return False
    for g in x:
        if g not in (0, 1):
            return False
    return True
