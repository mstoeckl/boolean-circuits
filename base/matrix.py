# size is N x (I + 2 + N). 38 x (6 + 2 + 38) =
class Matrix():
    # NOTE: so far, impl. only works for symettric functions.
    # let's leave it at that for now: we can always tack on a fancy
    # numbering scheme

    # 2
    # 1
    # 0
    # F
    # T
    # 0
    # 1
    # 2
    # 3
    # 4
    #

    def __init__(self, inputs):
        # inputs IxN
        self.I = [[] for _ in range(inputs)]
        # bool lines 2xN
        self.TF = [[],[]]
        # interform. NxN
        self.D = []
        # functions. N
        self.F = []
    def copy(self):
        mtx = Matrix(self.width())
        mtx.I = [x.copy() for x in self.I]
        mtx.TF = [x.copy() for x in self.TF]
        mtx.D = [x.copy() for x in self.D]
        mtx.F = self.F.copy()
        return mtx

    def swap_lines(self, i, j):
        raise NotImplemented() # TODO
    def sort(self):
        # create a toposort? Or is M->Dag->M good enough?
        raise NotImplemented() # TODO
    def __getitem__(self, i):
        return (tuple(v[i] for v in self.I), (self.TF[0][i],self.TF[1][i]),
               tuple(v[i] for v in self.D), self.F[i])
    def __delitem__(self, i):
        for x in self.I:
            del x[i]
        for x in self.TF:
            del x[i]
        del self.F[i]
        for x in self.D:
            del x[i]
        del self.D[i]
    def merge_lines(self, fr, to):
        for x in range(len(self)):
            if self.D[fr][x]:
                self.D[to][x] = self.D[fr][x]
        del self[fr]

    def add_blank(self):
        for k in self.I:
            k.append(0)
        for k in self.TF:
            k.append(0)
        self.D.append([0] * len(self.D))
        for k in self.D:
            k.append(0)
        self.F.append(None)
        return len(self.F) - 1

    def add(self, func, simples, callers):
        # careful; simples/callers are generators,
        # can't be iterated through twice
        i = self.add_blank()
        for x in simples:
            if x == "T":
                self.TF[1][i] = 1
            elif x == "F":
                self.TF[0][i] = 1
            else:
                self.I[x][i] = 1
        for x in callers:
            self.D[i][x] = 1
        self.F[i] = func
        return i
    def __len__(self):
        return len(self.F)
    def width(self):
        return len(self.I)

    def __call__(self, args):
        """ Line 0 yields the output. """

        # a really efficient way:
        # pull topological sort, set a flag; if sorted,
        # just eval R-to-L
        # sort-check is easy, just matrix upper triangle scan

        # an easy way, without sorting

        # we start at 0
        g = [-1] * len(self)
        l = range(len(self))
        w = range(self.width())
        def rxv(i, g, self):
            if g[i] != -1:
                return g[i]

            # TODO: Bottleneck
            v = [rxv(j, g, self) for j in l if self.D[j][i]]
            if self.TF[0][i]:
                v.append(0)
            if self.TF[1][i]:
                v.append(1)
            v += [args[j] for j in w if self.I[j][i]]
            g[i] = self.F[i](v)
            return g[i]

        v = rxv(0, g, self)
        return v

    def __repr__(self):
        return "<Matrix: {}:{}>".format(len(self.I), len(self.F))

# Operators

def matrix_combine(op, *mtxs):
    """
    Takes a list of Matrices, and creates a new
    one whose result is the op applied to the results
    of the Matrices.
    """
    # =====
    # |X
    # | XX
    # | XX
    # |   X
    raise NotImplemented() # TODO

def matrix_crosslink(mtx):
    """
    Crosslink a matrix, so no forms are double-evaluated
    """
    mtx = mtx.copy()

    # O(n**2), and fails on nested crosslinks

    s = {}
    i = 0
    mgmap = {}
    while i < len(mtx):
        l = mtx[i]
        if l in s:
            mgmap[i] = s[l]
        else:
            s[l] = i
        i += 1

    for fr,to in sorted(mgmap.items(), key=lambda x:x[0], reverse=True):
        mtx.merge_lines(fr, to)

    return mtx

def matrix_print(mtx):
    l = len(mtx)
    tf = lambda x:"⋅∎"[x]
    fts = "{{:>{}}} ".format(len(str(l)))
    prep = " "*(len(str(l)) - 1)
    s = []
    empty = lambda x: sum(x) == 0

    if not empty(mtx.TF[0]) or not empty(mtx.TF[1]):
        s.append(prep+"  "+"-"*l)
        if not empty(mtx.TF[0]):
            s.append(prep+"F "+"".join(map(tf, mtx.TF[0])))
        if not empty(mtx.TF[1]):
            s.append(prep+"T "+"".join(map(tf, mtx.TF[1])))
    s.append(prep+"  "+"-"*l)
    g = False
    for i in range(len(mtx.I)):
        if not empty(mtx.I[i]):
            g = True
            s.append(fts.format(i) + "".join(map(tf, mtx.I[i])))
    if g:
        s.append(prep+"  "+"-"*l)
    s.append(prep+"  "+"".join(map(lambda x:str(x)[1], mtx.F)))
    s.append(prep+"  "+"-"*l)
    g = False
    for i in range(len(mtx.D)):
        if not empty(mtx.D[i]):
            g = True
            s.append(fts.format(i) + "".join(map(tf, mtx.D[i])))
    if g:
        s.append(prep+"  "+"-"*l)

    return "\n".join(s)

# Conversion
from .functions import *
from .dag import *
from .data import *

MATRIX = "MATRIX"

def dag2matrix(dag):
    if dag == "T":
        d = Dag(ID, "T")
    elif dag == "F":
        d = Dag(ID, "F")
    elif isinstance(dag, int):
        d = Dag(ID, dag)
    else:
        d = dag

    m = Matrix(d.get_width())
    for x in d.iter_H():
        x.v = m.add(x.F, filter(lambda x: not isinstance(x, Dag), x.L),
            map(lambda x: x.v, x.post))
    d.__exit__(None,None,None)

    return m

def matrix2dag(mtx):
    l = len(mtx)
    g = [None] * l

    w = mtx.width()
    rw = range(w)
    rl = range(l)
    def dg(i):
        if g[i] is not None:
            return g[i]
        v = mtx[i]
        m = [i for i in range(w) if v[0][i]]
        if v[1][0]:
            m.append("F")
        if v[1][1]:
            m.append("T")
        m += [dg(i) for i in range(l) if v[2][i]]
        j = Dag(v[3], *m)
        g[i] = j
        return j

    return dg(0)

def isMatrix(m):
    return isinstance(m, Matrix)

def matrix2data(mtx):
    return Data(mtx(v) for v in boolvecs(mtx.width()))

from . import conv

conv.conversions[conv.CIRCUIT][MATRIX] = dag2matrix
conv.conversions[MATRIX] = {conv.CIRCUIT:matrix2dag,
                            conv.DATA:matrix2data}
conv.identifiers[MATRIX] = isMatrix
