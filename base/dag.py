class Dag():
    def __init__(self, func, *preobjs):
        self.F = func
        self.L = list(preobjs)
        for x in preobjs:
            if isinstance(x, Dag):
                x.post.append(self)
        self.post = []
        self.v = None

    # Safety!

    def __enter__(self):
        if self.v is not None:
            raise Exception("Dag is not clean")
        for x in self.iter_dag():
            x.__enter__()
        return self

    def __exit__(self, type, value, traceback):
        if type is not None:
            return False

        self._markAll("X") # "X" is never used as a value
        self._markAll(None)

        return False
    def _markAll(self, val):
        self.v = val
        for x in self.iter_dag():
            if x.v != val:
                x._markAll(val)

    # Call

    def __call__(self, args):
        with self as _:
            return self._call(args)
    def _call(self, args):
        if self.v is not None:
            return self.v

        v = []
        for x in self.L:
            if isinstance(x, Dag):
                v.append(x._call(args))
            elif x == "T":
                v.append(1)
            elif x == "F":
                v.append(0)
            else:
                v.append(args[x])
        self.v = self.F(v)
        return self.v

    # Details

    def get_size(self):
        # call/reset style? or set accumulate
        pass

    def iter_dag(self):
        for x in self.L:
            if isinstance(x, Dag):
                yield x

    def unbind(self, pred):
        self.L.remove(pred)
        pred.post.remove(self)

    def __repr__(self):
        return "⎛{} {}⎠".format(self.F, " ".join(map(repr, self.L)))

    def get_width(self):
        m = 0
        for x in self.L:
            if isinstance(x, Dag):
                m = max(m, x.get_width())
            elif isinstance(x, int):
                m = max(m, x+1)
        return m

    # Copy

    def copy(self):
        with self as _:
            return self._copy()
    def _copy(self):
        # builds a parallel skeleton, returns head
        if self.v is None:
            sub = []
            for x in self.L:
                if isinstance(x, Dag):
                    sub.append(x._copy())
                else:
                    sub.append(x)
            self.v = Dag(self.F, *sub)
        return self.v

    # Iter.

    def iter_H(self):
        """ Thou shalt not change me whilst iterating! """
        def iterMe(self):
            if self.v is None:
                self.v = True
                for u in self.post:
                    yield from iterMe(u)
                yield self
                for x in self.iter_dag():
                    yield from iterMe(x)

        with self as self:
            yield from iterMe(self)


    # Transform

    def transform_L(self, formfunc, inputfunc=lambda x:x, constfunc=lambda x:x):
        ibank = tuple(inputfunc(x) for x in range(self.get_width()))
        cfalse = constfunc("F")
        ctrue = constfunc("T")
        def sub(u):
            if isinstance(u, int):
                return ibank[u]
            elif u == "T":
                return cfalse
            elif u == "F":
                return ctrue

            if u.v is not None:
                return u

            for i in range(len(u.L)):
                u.L[i] = sub(u.L[i])

            u.v = True

            pp = u.post.copy()
            u.post = []

            z = formfunc(u)

            if isinstance(z, Dag):
                z.post = pp
                z.v = True

            for x in pp:
                for k in range(len(x.L)):
                    if x.L[k] == u:
                        x.L[k] = z

            return z

        g = self.copy()
        g.__enter__()
        u = sub(g)
        u.__exit__(None, None, None)
        return u

    def __eq__(self, o):
        return isinstance(o, Dag) and self.L == o.L and self.F == o.F

    def __hash__(self):
        return hash(tuple(self.L)) ^ hash(self.F)

def dag_call(x, args):
    if isinstance(x, Dag):
        return x(args)
    if x == "T":
        return 1
    elif x == "F":
        return 0
    else:
        return args[x]

def dag_width(x):
    if isinstance(x, Dag):
        return x.get_width()
    if x == "T":
        return 0
    elif x == "F":
        return 0
    else:
        return x + 1

def dag_transform_L(x, form, inp=lambda x:x, const=lambda x:x):
    if isinstance(x, Dag):
        return x.transform_L(form, inp, const)
    if x in "TF":
        return const(x)
    else:
        return inp(x)

def dag_head(x):
    if isinstance(x, Dag):
        return x.F
    return x
