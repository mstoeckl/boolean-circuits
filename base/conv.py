from .aux import *
from .functions import *
from .formula import *
from .dag import *
from .data import *
from .text import *

## This section deals with all operations relating to text2formula

def formula2circuit(f):
    if not isForm(f):
        return f

    return Dag(head(f), *map(formula2circuit, tail(f)))

def circuit2data(g):
    z = Data(dag_call(g, v) for v in boolvecs(dag_width(g)))
    return z

def data2formula(d):
    # WARNING: kinda dirty, since it involves a base
    # takes data list
    dat = list(d)
    l = len(dat)
    if l == 1:
        if dat[0]:
            return "T"
        else:
            return "F"

    if l == 2:
        if dat[0] and dat[1]:
            return "T"
        if not dat[0] and dat[1]:
            return 0
        if dat[0] and not dat[1]:
            return (NOT, 0)
        if not dat[0] and not dat[1]:
            return "F"

    n = clog2(l)

    s = []
    for i in range(l):
        if dat[i]:
            v = int2binl(i, n)
            g = []
            for x in range(n):
                if not v[x]:
                    g.append((NOT, x))
                else:
                    g.append(x)
            s.append(xtuple(AND, g))

    if len(s) == 0:
        return "F"
    elif len(s) == 1:
        return s[0]
    else:
        return xtuple(OR, s)

TEXT = "TEXT"
FORMULA = "FORMULA"
CIRCUIT = "CIRCUIT"
DATA = "DATA"

# TODO: fully abstract this conversion logic, so stuff like
# TEXT, FORMULA, DATA builds off it.

# TEXT -> FORMULA -> DATA -> CIRCUIT -> MATRIX

conversions = {TEXT:{FORMULA:text2formula},
        FORMULA:{TEXT:formula2text,CIRCUIT:formula2circuit},
        CIRCUIT:{DATA:circuit2data},
        DATA:{FORMULA:data2formula}
        }

def convertB(val, fr, to):
    """ A generic conversion function between different types"""
    # arbitrary graph for conversions
    if fr == to:
        return val

    for x in conversions.values():
        for y in x.values():
            y.visited = 0

    found = [[fr]]
    soln = None
    seeking = True
    while seeking and found:
        nxt = []
        for u in found:
            branch = conversions[u[-1]]
            for n in branch:
                if branch[n].visited:
                    continue
                branch[n].visited = 1

                nxt.append(u + [n])
                if n == to:
                    soln = u + [n]
                    seeking = False
        found = nxt

    if soln == None:
        raise Exception("No such conversion found")

    for i in range(1, len(soln)):
        val = conversions[soln[i-1]][soln[i]](val)

    return val


identifiers = {TEXT:lambda x:
                   isinstance(x, str) and x not in "TF",
               FORMULA:isFormula,
               CIRCUIT:lambda x:
                   isinstance(x, Dag),
               DATA:isData}

def typeOf(thing):
    for x,v in identifiers.items():
        if v(thing):
            return x

    raise Exception("Unidentified type {}".format(thing))

def convert(val, *path):
    """
    convert is base and limitation agnostic. It assumes dumb AOI∞/conjunctive normal form
    """
    if path:
        start = typeOf(val)
        val = convertB(val, start, path[0])
        for pair in pairs(path):
            val = convertB(val, pair[0], pair[1])
    return val

__all__ = ["convert", "TEXT", "FORMULA","CIRCUIT", "DATA"]
